#!/usr/bin/python
# -*- coding: utf-8 -*-

# se importa la libreria json, 
# se utilizará para el manejo de datos de los aminoacidos
import json

# Se llama al ingresar, modificar o eliminar aminoacidos
def save_file(aminoacidos):

    with open("AA.json", 'w') as file:
        json.dump(aminoacidos, file)

# Se llama al cargar la base de datos creada
def load_file():

    with open("AA.json", 'r') as file:
        aminoacidos = json.load(file)
    return aminoacidos

# Lista la base de datos creada
def impresora():
    aminoacidos = load_file()
    texto1 = "Aminoácido"
    texto2 = "Formula Molecular"
    print("|{0:^15}|{1:^17}|".format(texto1, texto2))
    for key, value in aminoacidos.items():
        print("|{0:^15}|{1:^17}|".format(key, value))

# Elimina un aminoácido a petición del usuario
def eliminar():

    aminoacidos = load_file()
    delAA = input("Ingrese el nombre del aminoácido a eliminar").upper()

    existe = aminoacidos.get(delAA, 'El elemento no existe')

    if existe:
        del aminoacidos[delAA]
        save_file(aminoacidos)
        print(aminoacidos)

# Edita un aminoácido a petición del usuario
def editar():

    aminoacidos = load_file()
    editAA = input("ingrese el aminoácido: ").upper()

    existe = aminoacidos.get(editAA, 'El elemento no existe')

    if existe:
        aminoacidos[editAA] = input("Ingrese una nueva formula").upper()

    save_file(aminoacidos)

# Genera una lista con los aminoacidos que pertenecen al mismo atomo
def fusiona_diccionario(aa, aa1):

    aatemporal = {**aa, **aa1}
    for key, value in aatemporal.items():
        if key in aa and key in aa1:
            aatemporal[key] = [value, aa[key]]

    return aatemporal

# Separa la Formula molecular del aminoácido en atomos, 
# para posteriomente que estos sean keys de las coincidencias de aminoácidos
def separa_atomos(a):

    x = []
    for value in a.values():
        y = [' ' + i if i in 'CHONS' and value.index(i) != 0 else i for i in value]
        y = "".join(y)
        y = y.split()
        x.append(y)
    return x

# busca por formula completa o atomica si el query del usuario existe
def buscador():


    aminoacidos = load_file()
    aa = {}
    for j in range(len(aminoacidos)):
        x = separa_atomos(aminoacidos)
        aa1 = {x[j][i]: str(list(aminoacidos.keys())[j]) for i in range(len(x[j]))}
        aa = fusiona_diccionario(aa, aa1)
    busqueda = input("Ingrese su busqueda: ")
    invierteKV = {value: key for key, value in aminoacidos.items()}
    print(aa.get(busqueda, "no se encuentra en busqueda de atomos, se busca en toda la formula"))
    if not aa.get(busqueda, False):
        print(invierteKV.get(busqueda, "no se encuentra en busqueda completa"))

# Agrega nuevos aminoacidos a la base de datos
def aminoacido():

    aminoacidos = load_file()
    agregaAA = {input("Nombre del AA: ").upper(): input("Formula molecular del AA ").upper()}
    aminoacidos.update(agregaAA)
    save_file(aminoacidos)

# Menu le piermite interactual al usuario con el programa, 
# esta llama a las funciones y finaliza con el numero 6, 
# cualquier otro comando continua el ciclo
def menu():
    
    texto = "1.Ingresar AA\n2.Busqueda\n3.Eliminar\n" \
            "4.Editar\n5.Listar aminoácidos\n6.Salir\n"
    while True:
        try:
            seleccion = int(input(texto))
            if seleccion == 1:
                aminoacido()
            if seleccion == 2:
                buscador()
            if seleccion == 3:
                eliminar()
            if seleccion == 4:
                editar()
            if seleccion == 5:
                impresora()
            if seleccion == 6:
                exit("Puede revisar su trabajo en AA.json, Gracias.")
        except ValueError:
            print("Ingrese un número en el menú por favor")
        else:
            print("Ingrese un número en el menú por favor")

# Main llama a menú e inicia el programa
if __name__ == '__main__':
    menu()
