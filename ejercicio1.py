#!/usr/bin/python
# -*- coding: utf-8 -*-

#diccionario que tontiene la informacion para el ejercicio
dic = {"Histidine": "C6H9N3O2"}

# Se encarga de que independiende de la
# cantidad de atomos de la molecula, esta será
# separada de manera que el atomo acompañe su cantidad
def separa_atomos(a):

    x = []
    for value in a.values():
        y = [' ' + i if i in 'CHONS' and value.index(i) != 0 else i for i in value]
        y = "".join(y)
        y = y.split()
        x.append(y)

    return x

#se crea un diccionario por comprension con los
# valores intercambiados, luego lo imprime
def intercambia_keyvalue():
    aa = {}
    for j in range(len(dic)):

        x = separa_atomos(dic)
        aa = {x[j][i]: str(list(dic.keys())[j]) for i in range(len(x[j]))}

    print(aa)

# main llama a intercambia_keyvalue para dar comienzo al programa
if __name__ == '__main__':

    intercambia_keyvalue()
