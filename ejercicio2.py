#!/usr/bin/python
# -*- coding: utf-8 -*-

# en los datos se separan los paises grandes en estados,
# ésta función genera una sumatoria de sus estadisticas dejando solo el país
def formatea_datos(datos):

    for y in range(1, len(datos), 1):
        if datos[y][0] == datos[y - 1][0]:
            datos[y][1] = datos[y][1] + datos[y - 1][1]
            datos[y][2] = datos[y][2] + datos[y - 1][2]
            datos[y][3] = datos[y][3] + datos[y - 1][3]
            datos[y - 1] = 'a'

    datos = [datos[i] for i in range(len(datos)) if datos[i][0] != 'a']

    return datos

#extrae datos de la base de datos del dia 29, esta contiene los del dia 28
def genera_datos():

    archivo = open("covid_19_data.csv")
    datos = []
    for i, linea in enumerate(archivo):
        linea = linea.replace('.0', '').split(',')
        #se cambia el tipo de variable a entero para facilitar calculos
        if i > 0:
            linea[len(linea) - 3] = int(linea[len(linea) - 3])
            linea[len(linea) - 2] = int(linea[len(linea) - 2])
            linea[len(linea) - 1] = int(linea[len(linea) - 1])
            #se deja solo el dia mas actualizado, asi evitamos redundar en paises
        if linea[1] == '03/28/2020':
            del(linea[0:3])
            del (linea[1])
            datos.append(linea)
    archivo.close()
    return datos

if __name__ == '__main__':


    datos = genera_datos()
    datos.sort()
    datos = formatea_datos(datos)

    texto = "pais {0:20} contagiados {1:6} muertos más recuperados {2}"

    # imprime los datos requeridos en el ejercicio
    for i in range(len(datos)):
        print(texto.format(datos[i][0], datos[i][1], datos[i][2] + datos[i][3]))
    print("Cantidad de paises con al menos un contagiado", len(datos))
